package com.example.newdemo;

import com.example.newdemo.dao.CustomerRepository;
import com.example.newdemo.models.Customer;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
class NewDemoApplicationTests {

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void AddCustomer(){
        customerRepository.save(new Customer("Rick", "Grimes", "rick@null.com", "123 Fake Street", "Nashville", "TN", "73011"));
        //assertThat(customerRepository.findById(1L)).isInstanceOf(Optional.class);
    }

}
