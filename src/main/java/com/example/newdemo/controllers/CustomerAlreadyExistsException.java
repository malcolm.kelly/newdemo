package com.example.newdemo.controllers;

public class CustomerAlreadyExistsException extends RuntimeException{
    CustomerAlreadyExistsException(String first, String last){
        super("" + first + " " + last + " is already in the list");
    }
}
