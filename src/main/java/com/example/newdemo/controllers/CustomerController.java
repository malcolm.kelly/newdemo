package com.example.newdemo.controllers;


import com.example.newdemo.dao.CustomerRepository;
import com.example.newdemo.models.Customer;
import com.example.newdemo.services.CustomerService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class CustomerController {
    private final CustomerRepository respository;
    private final CustomerService service;

    CustomerController(CustomerRepository respository, CustomerService service){
        this.respository = respository;
        this.service = service;
    }

    @GetMapping("/customers/all")
    List<Customer> all(){
        return respository.findAll(Sort.by("firstName"));
    }

    @GetMapping("/customers/{id}")
    Customer getCustomerById(@PathVariable Long id){
        return respository.findById(id).orElseThrow(() -> new CustomerNotFoundException(id));
    }

    @GetMapping("/customers")
    List<Customer> listByCity(@RequestParam String city){
        return respository.findByCity(city);
    }

    @PostMapping(path = "/customers")
    public ResponseEntity<Customer> addCustomer(@RequestBody Customer newCust) {
        Customer added;
        if(service.isCustomerNotInList(newCust)){
            added = respository.save(newCust);
        }else{
            throw new CustomerAlreadyExistsException(newCust.getFirstName(), newCust.getLastName());
        }
        return new ResponseEntity<Customer>(added, HttpStatus.OK);
    }

    @PutMapping(path = "/customers/{id}/edit", consumes = "application/x-www-form-urlencoded")
    public ResponseEntity<Customer> editCustomer(@PathVariable("id") Customer oldCust, Customer newCust){
        //BeanUtils.copyProperties(newCust, oldCust, "id");
        oldCust.setFirstName(newCust.getFirstName());
        oldCust.setLastName(newCust.getLastName());
        oldCust.setEmailAddress(newCust.getEmailAddress());
        oldCust.setHomeAddress(newCust.getHomeAddress());
        oldCust.setCity(newCust.getCity());
        oldCust.setState(newCust.getState());
        oldCust.setZipCode(newCust.getZipCode());
        return ResponseEntity.ok(respository.save(oldCust));
    }


    @RequestMapping(method = RequestMethod.POST, path = "/customers/upload")
    List<Customer> printCustomers() throws IOException {
        return service.insertCustomerList("data/customerlist.txt");
    }

    @GetMapping(path="/customers/delete/{id}")
    public List<Customer> deleteCustomerById(@PathVariable long id){
        respository.findById(id).orElseThrow(() -> new CustomerNotFoundException(id));
        respository.deleteById(id);
        return respository.findAll();
    }


}
