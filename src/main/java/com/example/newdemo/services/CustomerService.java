package com.example.newdemo.services;


import com.example.newdemo.dao.CustomerRepository;
import com.example.newdemo.models.Customer;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;


@Service
public class CustomerService {

    private final CustomerRepository repo;

    public CustomerService(CustomerRepository repo) {
        this.repo = repo;
    }


    /**
     * A boolean function to check if a new customer from the parameter
     * is already in the list
     * @param customer the customer to be inserted
     * @return true if the customer is not in the list, false if it already is
     */
    public boolean isCustomerNotInList(Customer customer){
        List<Customer> searchCust = repo.findCustomerByFirstNameAndLastNameAndEmailAddress(customer.getFirstName(), customer.getLastName(), customer.getEmailAddress());
        boolean exists = searchCust.isEmpty();
        return exists;
    }

    /**
     * Inserts a whole list of customers from a file
     * @param list the file of customers
     * @return new list of customers updated
     * @throws IOException
     */
    public List<Customer> insertCustomerList(String list) throws IOException {
        InputStream is = new ClassPathResource(list).getInputStream();
        try{
            String contents = new String(FileCopyUtils.copyToByteArray(is), StandardCharsets.UTF_8);
            String[] lines = contents.split("\\r?\\n");
            for(String line : lines){
                String[] info = line.split(":");
                String first = info[0];
                String last = info[1];
                String email = info[2];
                String home = info[3];
                String city = info[4];
                String state = info[5];
                String zip = info[6];

                Customer cust = new Customer(first, last, email, home, city, state, zip);
                if(isCustomerNotInList(cust)){
                    repo.save(cust);
                }
            }

        } catch (IOException e){
            e.printStackTrace();
        }
        return repo.findAll();
    }



}
