package com.example.newdemo.dao;


import com.example.newdemo.models.Customer;
import com.example.newdemo.services.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseTester {
    private static final Logger log = LoggerFactory.getLogger(DatabaseTester.class);

    @Bean
    CommandLineRunner init(CustomerRepository respository, CustomerService service){
        Customer test = new Customer("Rick", "Grimes", "rick@null.com", "4897 Green Street", "Nashville", "TN", "37211");
        return args -> {
            // returns the full list; if empty, add the following list to the database
            log.info("Preloading " + service.insertCustomerList("data/customers.txt"));

            // tests the repo to see if it can add the test customer
            if(service.isCustomerNotInList(test)){
                log.info("Preloading " + respository.save(test));
            }
            else{
                log.info("Customer is already in");
            }
        };
    }
}
