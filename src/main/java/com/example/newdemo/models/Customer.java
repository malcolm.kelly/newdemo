package com.example.newdemo.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String emailAddress;

    @Column(name = "home")
    private String homeAddress;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "zip")
    private String zipCode;


    public Customer(String firstName,
                    String lastName,
                    String emailAddress,
                    String homeAddress,
                    String city,
                    String state,
                    String zipCode){
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.homeAddress = homeAddress;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
    }

    public Customer() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public boolean equals(Object o){
        if(this == o){
            return true;
        }
        if(!(o instanceof Customer))
            return false;
        Customer customer = (Customer) o;
        return Objects.equals(this.firstName, customer.firstName)
                && Objects.equals(this.lastName, customer.lastName)
                && Objects.equals(this.emailAddress, customer.emailAddress);
    }

    @Override
    public String toString(){
        return "Customer {" + "id=" + this.id +
                ", first name:'" + this.firstName +
                "', last name: '" + this.lastName +
                "' email address: '" + this.emailAddress +
                "' home address: '" + this.homeAddress +
                "' city: '" + this.city +
                "' state: '" + this.state + "' }";
    }


}
